import { Container } from "react-bootstrap";
import "../Styles/Ticket.scss";

export default function Ticket(props) {
    return (
        <Container className="ticket">
            <h2 className="name-ticket">{props.name}</h2>
            <p className="details-ticket">
                {props.details}
            </p>
        </Container>
    );
}