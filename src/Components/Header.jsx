import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar, Nav } from 'react-bootstrap';

export default function Header() {
    return (
        <header id="header">
            <Navbar bg="dark" variant="dark" expand="lg" className="p-2">
                <Navbar.Brand href="#home">Handi-Ref</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ml-auto">
                        <Nav.Link href="/">Home</Nav.Link>
                        <Nav.Link href="/report">Report</Nav.Link>
                        <Nav.Link href="/about">Certified</Nav.Link>
                        <Nav.Link href="/profil">Profil</Nav.Link>
                        
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </header>
    );
}