import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Header from './Components/Header';
import Home from './Pages/Home';
import Profil from './Pages/Profil';
import FormPage from './Pages/Formpage';
import Certifier from './Pages/Certifier'; // Importez le composant Certifier

function App() {
  return (
    <div className="app">
      <BrowserRouter>
        <Header />
        <Routes>
          <Route exact path='/' element={<Home />} />
          <Route exact path='/profil' element={<Profil />} />
          <Route exact path='/report' element={<FormPage />} />
          <Route exact path='/about' element={<Certifier />} /> 
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
