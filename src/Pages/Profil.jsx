import { Container } from "react-bootstrap";
import "../Styles/Profil.scss";
import Ticket from "../Components/Ticket";

export default function Profil() {
    return (
        <Container fluid className="profil">
            <Container fluid className="identity">
                <Container className="container-profil-image">
                    <img src="./profil.png" alt="" />
                </Container>
                <Container className="details">
                    <p>
                        Name : Marc RASANDISON <br />
                        Site : <a href="https://mycv.rasandison-apps.com/" target="blanck">rasandison-apps.com</a>
                    </p>
                </Container>
            </Container>

            <Container className="tickets">
                <h1 className="h1-ticket">Tickets</h1>
                <Ticket name="Daltonien vert" details="Jeg er farveblind, og jeg kan ikke se grønt. Jeg er nødt til at skifte til orange." />
                <Ticket name="Contraste" details="Le contraste des couleurs me pique les yeux" />
            </Container>
        </Container>
    );
}