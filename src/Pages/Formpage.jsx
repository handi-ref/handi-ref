import React, { useState } from 'react';
import '../Styles/Formpage.css'; 

const FormPage = () => {
  const [pseudo, setPseudo] = useState('');
  const [email, setEmail] = useState('');
  const [category, setCategory] = useState('');
  const [otherCategory, setOtherCategory] = useState('');
  const [website, setWebsite] = useState('');
  const [description, setDescription] = useState('');

  const handleSubmit = (event) => {
    event.preventDefault();
    // Logique de traitement du formulaire ici
  };

  const handleCategoryChange = (event) => {
    const selectedCategory = event.target.value;
    setCategory(selectedCategory);
    if (selectedCategory !== 'other') {
      setOtherCategory('');
    }
  };

  return (
    <div className="form-page-container">
      <div className="form-page-content">
        <h2 className="form-page-title">Report</h2>
        <form onSubmit={handleSubmit} className="form-page-form">
          <div className="form-group">
            <label htmlFor="pseudo">Pseudo:</label>
            <input
              type="text"
              id="pseudo"
              className="form-control"
              placeholder="Entrez votre pseudo"
              value={pseudo}
              onChange={(e) => setPseudo(e.target.value)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="email">Email:</label>
            <input
              type="email"
              id="email"
              className="form-control"
              placeholder="Entrez votre email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="category">Catégorie:</label>
            <select
              id="category"
              className="form-control"
              value={category}
              onChange={handleCategoryChange}
            >
              <option value="">Sélectionnez une catégorie</option>
              <option value="myopie">Myopie</option>
              <option value="daltonisme">Daltonisme</option>
              <option value="other">Autre</option>
            </select>
          </div>
          {category === 'other' && (
            <div className="form-group">
              <label htmlFor="otherCategory">Autre catégorie:</label>
              <input
                type="text"
                id="otherCategory"
                className="form-control"
                placeholder="Entrez une autre catégorie"
                value={otherCategory}
                onChange={(e) => setOtherCategory(e.target.value)}
              />
            </div>
          )}
          <div className="form-group">
            <label htmlFor="website">Liens du site:</label>
            <input
              type="text"
              id="website"
              className="form-control"
              placeholder="Entrez le lien de votre site"
              value={website}
              onChange={(e) => setWebsite(e.target.value)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="description">Description:</label>
            <textarea
              id="description"
              className="form-control"
              placeholder="Entrez une description"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            />
          </div>
          <button type="submit" className="btn btn-primary">Valider</button>
        </form>
      </div>
    </div>
  );
};

export default FormPage;
