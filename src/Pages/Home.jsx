import { Button, Container } from "react-bootstrap";
import "../Styles/Home.scss"

export default function Home() {
    return (
        <Container fluid className="home">
            <Container className="home-image">
                <img src="./handi-numerique.jpg" alt="" />
            </Container>
            <Container className="presentation">
                <p>
                    Notre plateforme de crowdsourcing est un espace collaboratif en ligne où les individus peuvent contribuer à des projets et des initiatives d'envergure en apportant leurs compétences, leurs idées et leur expertise. Nous offrons une plateforme dynamique et inclusive qui rassemble une communauté diversifiée de participants passionnés, prêts à relever des défis et à faire une réelle différence. Que vous soyez un développeur, un designer, un rédacteur, un spécialiste du marketing ou simplement quelqu'un qui souhaite contribuer, notre plateforme est l'endroit idéal pour vous engager activement dans des projets innovants. En collaborant avec d'autres membres de la communauté, vous aurez la possibilité de partager vos connaissances, de participer à des projets à fort impact et de repousser les limites de l'innovation. Rejoignez-nous dès aujourd'hui et contribuez à façonner un avenir meilleur grâce à la puissance du crowdsourcing.
                </p>
            </Container>
            <Button onClick={() => window.location.href = '/report'}>Let's try</Button>
        </Container>
    );
}