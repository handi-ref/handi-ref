import React from 'react';

const Certifier = () => {
  const certifiedSites = [
    { name: 'Apple', rating: 4 },
    { name: 'Tesla', rating: 5 },
    { name: 'Cv Marc', rating: 3 },
    // Ajoutez d'autres sites certifiés ici
  ];

  return (
    <div>
      <h2>Certified Sites</h2>
      <ul>
        {certifiedSites.map((site, index) => (
          <li key={index}>
            <span>{site.name}</span>
            <span>{'★'.repeat(site.rating)}</span>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Certifier;
